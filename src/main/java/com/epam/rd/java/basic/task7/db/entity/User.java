package com.epam.rd.java.basic.task7.db.entity;

import java.util.Objects;

public class User {

	private int id;

	private String login;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}
	
	public String toString() { return login.toString(); }

	public void setLogin(String login) {
		this.login = login;
	}

	public static User createUser(String login) { return new User(0,login); }
	
	public User(int id, String login){
		this.login = login;
		this.id = id;
	}
	public User(){
		this.login=login;
		this.id=id;
	}
	@Override
	public boolean equals(Object obj){
		User user = (User) obj;
		return Objects.equals(this.getLogin(),user.getLogin()); }

}
